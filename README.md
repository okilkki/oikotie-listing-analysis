# Inspect public transport routes in Oikotie listings

[OikotieHSLRoutes.ipynb](OikotieHSLRoutes.ipynb) is used to evaluate listings on [Oikotie](https://asunnot.oikotie.fi/myytavat-asunnot) using canopy/selenium, GraphQL & REST apis for geocoding and itinerary planning from [HSL](https://digitransit.fi/en/developers/apis/) with F#. Distance to Helsinki center on weekday mornings is used to sort available listings.
